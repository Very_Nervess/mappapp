# class for a single tile, has values that target a space on a grid indicating its target tile.
class Tile:
    def __init__(self, targetrow = 0, targetcol = 0):
        self.targetrow = targetrow
        self.targetcol = targetcol
#getters and setters:
    def get_targetcol(self):
        return self.targetcol

    def get_targetrow(self):
        return self.targetrow

    def set_targetcol(self,value):
        self.targetcol = value

    def set_targetrow(self,value):
        self.targetrow = value
    #override method, returns the tile's tatget as a string in the form : y,x
    def __str__(self):
        return str(self.targetrow)+","+str(self.targetcol)
