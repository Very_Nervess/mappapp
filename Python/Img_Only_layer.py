# an imageonly object that describes layers in the stack that contain only images. Used for free drawing or importing whole images into the map.
from PIL import Image
class Imageonly():
    def __init__(self,name,image):
        self.name=name
        self.image=image
    #name getter
    def get_name(self):
        return self.name
    #image getter
    def get_image(self):
        return self.image
    #size changer - pastes the old image into the top left corner of the newly sized transparent image.
    def set_size(self,width,height):
        newpaper = Image.new('RGBA', (width, height), (255,255, 0, 0))
        oldx=self.image.size[0]
        oldy=self.image.size[1]
        x=min(width,oldx)
        y=min(height,oldy)
        newpaper.paste(self.image)
        self.image=newpaper
