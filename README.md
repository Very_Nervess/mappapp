# mappapp

Required python (3.7) modules:

Kivy 1.10.1

PILLOW 5.3

Note: Unfortunatley broken file dependencies will likely cause the code to crash when run outside of my machine, its something I need to fix.

The revelant files are:

Frontend1.py 		- Contains all code for the GUI and controls the applications. A lot of this code is very messy and complcated and not well documented. Sorry =( 

Backend1.py 		- A class that controls the major functioning of the program and handles all processing 

Layerstack.py 		- A class describing an object that holds the information and methods required for drawing the map 

Drawer.py 			- A class that draws images based on the information stored in a layer. 

Masterdrawer.py 	- A class that draws an entire stack of Layers, and also handles drawing of a grid on the image. 

Layer.py 			- A class that inherits frome the canvas class - it describes a grid that holds information for each tile in the grid that will eventually become the map. 

Image_Only_layer.py - A class that contains an image object, as well as information to make it compatible for use in a Layerstack object. 

Canvas.py			- A basic class that contains a grid of tile objects.

Tilemap.py 			- A class that contains the tiletset, or reference image 

Tile.py 			- A very basic class - contains an x,y co-ordinate. This could probably just have been a tuple.

Other files in the directory arejust places where I tested out new things, or experimented with features. 
