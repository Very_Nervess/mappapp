# a canvas class - essentially just a grid of tiles with a specific width and height (in pixels) and a grid resolution (the size of each grid block).
from Tile import *
class Canvas:
    def __init__(self,width,height,gridresolution=64):
        self.height=height
        self.width=width
        self.gridresolution=gridresolution
        self.tiles=[]
        #code to populate the canvas with blanks tiles (they all point to 0,0) Tiles are stored in a list of lists where each inner list represents a column and the outer list represents the rows.
        numberofcolumns = width//self.gridresolution
        numberofrows = height//self.gridresolution
        for i in range(numberofcolumns):
            self.tiles.append([])
            for j in range(numberofrows):
                self.tiles[i].append(Tile())
    # method that updates a specific tile's target
    def set_tile(self,canvaspositionx,canvaspositiony,newtargetx,newtargety):
        self.tiles[canvaspositionx][canvaspositiony].set_targetcol(newtargetx)
        self.tiles[canvaspositionx][canvaspositiony].set_targetrow(newtargety)
    # method to extract a specific Tile object
    def get_tile(self,xposition,yposition):
        return self.tiles[xposition][yposition]
    # getters
    def get_numberofrows(self):
        numberofrows = self.height//self.gridresolution
        return numberofrows
    def get_numberofcolumns(self):
        numberofcolumns = self.width//self.gridresolution
        return numberofcolumns
    def get_height(self):
        return self.height
    def get_width(self):
        return self.width
    def get_gridres(self):
        return self.gridresolution
    #setters
    def set_width(self,width):
        self.width=width
    def set_height(self,height):
        self.height=height
