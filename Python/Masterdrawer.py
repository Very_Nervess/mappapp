# The master Drawer class takes a stack of layers, and draws them each in order resulting in a layered image. It can draw a grid on top of the resulting image as well.

from Drawer import *
from PIL import Image, ImageDraw
from Layerstack import *
class Masterdrawer:
    def __init__(self,canvasstack):
        self.stack = canvasstack

# Method that draws the map, draws a base canvas from the 0 position layer and then steps through the layer list, drawing each new layer on top of the base and all subsequent layers. If a layer is an image only layer it is drawn as well
    def drawstack(self):
        drawerbase = Drawer(self.stack.get_layer(0))
        basemap = drawerbase.drawmap()
        for layer in self.stack.get_layerlist():
            if isinstance(layer, Layer): #checks to see if a layer is an instance of a layer object, Image only layers also can exist in the stack.
                drawer = Drawer(layer)
                layermap = drawer.drawmap()
                basemap.paste(layermap,(0,0),layermap)
            else: #If a layer is an image only layer the whole image is added to the drawn map.
                img = layer.get_image()
                basemap.paste(img,(0,0),img)
        return basemap

    """ method that allows placing of tile images at any pixel co-ordinates - not restricted to a grid - redundant.
    def free_draw(self,layer, tile):
        #code to generate a 4 tupple containing the co-ordinates to crop out a single tile
        col = tile.get_targetcol()
        row = tile.get_targetrow()
        left = col*self.tilemap.get_gridres()
        upper = row*self.tilemap.get_gridres()
        right = left+self.tilemap.get_gridres()
        lower = upper+self.tilemap.get_gridres()
        box = (left,upper,right,lower)
        #code that extracts tile image
        tileimage = self.tilemap.get_tileimage(box)"""

    # method that draws a grid on a transparent image based on the dimensions of the layerstack and then copies it on top of that stack. Takes RGBA color format and the target image as parameters
    def draw_grid(self,image,color):
        grid = Image.new('RGBA', (self.stack.get_width(), self.stack.get_height()), (255, 0, 0, 0))
        gridres=self.stack.get_gridres()
        artist = ImageDraw.Draw(grid)
        # this section draws the vertical lines
        for i in range (self.stack.get_width()//gridres):
            artist.line([(i*gridres,0),(i*gridres,self.stack.get_height())], fill=color, width=1)
            if i>0:
                artist.line([(i*gridres-1,0),(i*gridres-1,self.stack.get_height())], fill=color, width=1)
        artist.line([(self.stack.get_width()-1,0),(self.stack.get_width()-1,self.stack.get_height())], fill=color, width=1)
        # this section draws the horizontal lines
        for i in range (self.stack.get_height()//gridres):
            artist.line([(0,i*gridres),(self.stack.get_width(),i*gridres)], fill=color, width=1)
            if i>0:
                artist.line([(0,i*gridres-1),(self.stack.get_width(),i*gridres-1)], fill=color, width=1)
        artist.line([0,(self.stack.get_height()-1),(self.stack.get_width(),self.stack.get_height()-1)], fill=color, width=1)
        # this section combines the images and closes the opened objects
        final = Image.alpha_composite(image,grid)
        del(artist)
        grid.close()
        return final
