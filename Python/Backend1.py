#The backend class is the brains of the application, all processing is done here based on information from the front end.
from Masterdrawer import *
import os
import io
import sys
import copy
from datetime import datetime
import base64

class Backend:
    def __init__(self):
        self.working_dir = self.get_dir()
        self.active_layer = 1
        self.active_tile_set = Tilemap(self.working_dir+'imgstore/tilesets/32bforrest.png',32)
        self.grid = True
        self.grid_color = (0,0,0,255)
        self.active_tile = Tile()
        self.create_drawspace(32,32)
        self.selectionarray=[]
        self.boxmode=-1
        self.fillmode= -1
        self.target_array=[]
        self.drawactivetile()
        self.undo_redo_array = []
        self.undo_redo_position = 0
        self.activetilsesize=(0,0)
#method that returns the directory of the currently running script:
    def get_dir(self):
        slash = "\\"
        full_path = sys.argv[0].split(slash)[0:-1]
        dir = ""
        for bit in full_path:
            dir += bit+"/"
        return dir


# method to update the current tileset.
    def set_active_tileset(self,file,gridres=32):
        self.active_tile_set=Tilemap(file,gridres)

# returns the size of the active tile. Unused right now.
    def getactivetilsesize(self):
        return self.activetilsesize

# The selection array is a list of selected tiles. This method clears it.
    def clearselectionarray(self):
        self.selectionarray=[]

    # sets the current active tile according to a KIVY mouse touuch co-ordinate
    def set_active_tile(self,mousetouch):
        gridpos=self.coord_convert_tileset(mousetouch)
        self.active_tile = Tile(gridpos[1],gridpos[0])

    # updates the targeted tile in the CURRENTLY active layer to the CURRENTLY active tile using a KIVY moust touch co-ordinate
    def update_target_tile(self,mousetouch):
        gridpos=self.coord_convert_map(mousetouch)
        self.layerlist.update_tile_in_layer(self.active_layer,gridpos[0],gridpos[1],self.active_tile.get_targetcol(),self.active_tile.get_targetrow())

    # method that updates a tile according to a grid position, not a mouse touch. Does not redraw the map to disk
    def update_target_tile_grid(self,gridpos):
        self.layerlist.update_tile_in_layer(self.active_layer,gridpos[0],gridpos[1],self.active_tile.get_targetcol(),self.active_tile.get_targetrow())

    #converts a KIVY mouse co-ordinate to a grid co-ordinate relative to the tileset size
    def coord_convert_tileset(self,touchpos):
        res = self.layerlist.get_gridres()
        touchx = touchpos[0]
        touchy = abs(touchpos[1] - self.active_tile_set.get_imagesize()[1])
        gridx = int(touchx//res)
        gridy = int(touchy//res)
        return (gridx,gridy)

    # this method handled the code for the fill tool - a tile is selected, and then the function recursively checks all adjacent tiles. If they match the original target tile they are updated, if not, the function stops.
    def fill_tool(self,touch):
        x=touch[0]
        y=touch[1]
        adjacents = ()
        current_layer = self.layerlist.get_layer(self.active_layer)
        this_tile_target = Tile(current_layer.get_tile(x,y).get_targetrow(),current_layer.get_tile(x,y).get_targetcol())
        print ("checkech",current_layer.get_tile(x,y),"ac",self.active_tile)
        if str(current_layer.get_tile(x,y))==str(self.active_tile):
            print ("Same fill selected")
            return "Same fill selected"
        upt = "none"
        left = "none"
        right = "none"
        bottom = "none"
        self.update_target_tile_grid(touch)
        print(this_tile_target)
        try:
            upt = current_layer.get_tile(x,max((y-1),-0.1))
            #adjacents.append(upt)
        except: print ("didn't find up")
        try:
            left = current_layer.get_tile(max((x-1),-0.1),y)
            #adjacents.append(left)
        except: print ("didn't find left")
        try:
            bottom = current_layer.get_tile(x,y+1)
            #adjacents.append(bottom)
        except: print ("didn't find down")
        try:
            right = current_layer.get_tile(x+1,y)
            #adjacents.append(right)
        except: print ("didn't find right")
        if str(upt)==str(this_tile_target):
            self.update_target_tile_grid(touch)
            self.fill_tool((x,y-1))
        if str(left)==str(this_tile_target):
            self.update_target_tile_grid(touch)
            self.fill_tool((x-1,y))
        if str(bottom)==str(this_tile_target):
            self.update_target_tile_grid(touch)
            y+=1
            self.fill_tool((x,y))
        if str(right)==str(this_tile_target):
            self.update_target_tile_grid(touch)
            x+=1
            self.fill_tool((x,y))

    # this method checks to see if a touch is in a boundry region between two grid cells. This is used to decresease the sensitivity of tile transitions.
    def check_boundry_touch(self,touchpos):
        res = self.layerlist.get_gridres()
        boundry = False
        touchx = touchpos[0]
        touchy = abs(touchpos[1] - self.active_tile_set.get_imagesize()[1])
        xbound = touchx % res
        ybound = touchy % res
        if res-res//10< xbound or xbound < res//10 or res-res//10< xbound or xbound < res//10:
            boundry = True
        return boundry

    #converts a KIVY mouse co-ordinate to a grid co-ordinate relative to the map size
    def coord_convert_map(self,touchpos):
        res = self.layerlist.get_gridres()
        touchx = touchpos[0]
        touchy = abs(touchpos[1] - self.layerlist.get_height())
        gridx = int(touchx//res)
        gridy = int(touchy//res)
        return (gridx,gridy)

    # A function that draws a sub grid of tiles into position on the main grid starting at a click and proceeding right and down:
    def draw_array(self,touch):
        x= touch[0]
        y= touch[1]
        for targetcol in self.target_array:
            y= touch[1]
            for target in targetcol:
                # the try is implimented incase the array would cause the program to try and update tiles beyond the bounds of the map.
                try:
                    self.layerlist.update_tile_in_layer(self.active_layer,x,y,target[0],target[1])
                except: pass
                y+=1
            x+=1


    # sets the current active layer
    def set_active_layer(self,number):
        self.active_layer = number


    # a method that creates a new map image, and layer stack
    def create_drawspace(self,width,height):
        bg = Layer("Background",width,height,self.get_active_tile_set(),self.get_active_tile_set().get_gridres())
        self.layerlist = Layerstack(bg)
        self.layerlist.new_layer(self.get_active_tile_set())
        self.layerlist.set_map_size(width,height)


    # This method actually draws the map as an image by getting the tile information from the entire layer stack.  It returns a file like memory object containing the byte information of the complete image.
    # I am working on the efficiency of the program, so the date-time print outs a simply used so I can get an idea of the runtime.
    def drawmap(self,undo_flag=False):
        starttime = datetime.now()
        artist = Masterdrawer(self.layerlist)
        currentmap = artist.drawstack()
        # if the grid option is slected, a grid is added to the image.
        if self.grid == True:
            currentmap = artist.draw_grid(currentmap,self.grid_color)
        endtime = datetime.now()
        print ("Tim elapsed ioncluding tobyte:",endtime.microsecond-starttime.microsecond,"*****************************")
        # this code handles converting the image to a byte string, which is wrapped in a bytesIO object to act as a file for use by the front end.
        byteobject = io.BytesIO()
        currentmap.save(byteobject, "PNG")
        byteobject.seek(0)
        byteimg = byteobject.read()
        dataBytesIO = io.BytesIO(byteimg)
        currentmap.close()
        endtime = datetime.now()
        print ("Tim elapsed ioncluding tobyte:",endtime.microsecond-starttime.microsecond,"*****************************")
        # this code is used to the und/redo feature. If this method is called the layerstack containing all of the information used to create the map image is stored.
        # up to 5 layerstacks can be stored at one time, after this the oldest stack is removed when a new one is added. This is a conservative measure as layerstack objects can take up a lot of memory.
        # if the method is called as part of an undo/redo the array is not updated.
        # The undo/redo function works simply by using the array of stored layer stacks and setting the current layerstack to a previous one if an undo is requested.
        # if a new call is made to draw when the current layerstack is not the most recent one, the array is cleared of all stacks following that stack.
        if undo_flag==False:
            print("Saving to ",self.undo_redo_position+1)
            if self.undo_redo_position < len(self.undo_redo_array)-1:
                self.undo_redo_array=self.undo_redo_array[0:self.undo_redo_position+1]
                self.undo_redo_position = len(self.undo_redo_array)-1
            if len(self.undo_redo_array)<6:
                self.undo_redo_array.append(copy.deepcopy(self.layerlist))
                self.undo_redo_position = len(self.undo_redo_array)-1
            else:
                self.undo_redo_array = self.undo_redo_array[1:len(self.undo_redo_array)]
                self.undo_redo_array.append(copy.deepcopy(self.layerlist))
                self.undo_redo_position = len(self.undo_redo_array)-1
        print (sys.argv[0])
        return dataBytesIO

    # this method draws the current active tile as an image and then saves it to disk for display by the front end. If an array of tiles is selected it draws the composite image resulting from stitching those tiles together.
    def drawactivetile(self):
        if len(self.target_array)<1:
            currenttile = self.active_tile_set.get_tileimage(self.active_tile)
            currenttile.save(self.working_dir+"imgstore/activetiledisplay.png")
            self.activetilsesize=currenttile.size
            currenttile.close()
        else:
            res= self.layerlist.get_gridres()
            width=len(self.target_array)*res
            height=len(self.target_array[0])*res
            tilearray = Image.new('RGBA', (width, height), (255, 0, 0, 0))
            for i,col in enumerate(self.target_array):
                for j,row in enumerate(col):
                    temptile=Tile(row[1],row[0])
                    tileimage = self.active_tile_set.get_tileimage(temptile)
                    tilearray .paste(tileimage,(i*res,j*res))
            tilearray.save(self.working_dir+"imgstore/activetiledisplay.png")
            self.activetilsesize=tilearray.size
            tilearray.close()

    # this method loads a layerstack from a text file. image layers are saved as 64b encoded byte strings, while normal layers are saved as lists of tile target co-ordinates.
    def load_from_file(self,file):
        seed=Layer("Background",32,32,self.get_active_tile_set(),self.get_active_tile_set().get_gridres())
        loadedstack=Layerstack(seed)
        source = open(file,'r')
        for line in source:
            if line[0]=='%':
                line = line[1:]
                rawdata=line.split("*")
                name = rawdata[1]
                width = rawdata[2]
                height = rawdata[3]
                bytestring = rawdata[4]
                bytes64 = bytestring.encode()
                bytes = base64.b64decode(bytes64)
                fauxfile = io.BytesIO(bytes)
                image = Image.open(fauxfile)
                loadedstack.add_image_layer(name,image)
            else:
                understand = line.split("*")
                tileset = Tilemap(understand[2],int(understand[5]))
                layer= Layer(understand[1],int(understand[3]),int(understand[4]),tileset,int(understand[5]))
                columns = understand[6][1:].split('$')
                for z, val in enumerate(columns):
                    print(z,val)

                    for i, col in enumerate(columns[:]):
                        tiles = col[1:].split('#')
                        for j,tile in enumerate(tiles[:]):
                            y=int(tile[0])
                            x=int(tile[2])
                            layer.set_tile(i,j,x,y)
                            print(i,j)
                loadedstack.add_layer(layer)
        loadedstack.del_layer(0)
        self.layerlist=loadedstack

    # method to save to a text file - this allows layer information to be preserved between sessions. Image only layers are saved as byte strings, other layers are saved as a string of co-ordinates relating to each tile.
    def save_to_file(self,file):
        pass
        target = open(file,'w')
        stack = self.layerlist.get_layerlist()
        for i, layer in enumerate(stack):
            if isinstance(layer,Layer):
                if i == len(stack)-1:
                    line = str(i)+"*"+layer.to_string()
                else:
                    line = str(i)+"*"+layer.to_string()+"\n"
            else:
                print(line)
                line = "%"+str(i)+"*"+layer.get_name()+"*"+str(layer.image.size[0])+"*"+str(layer.image.size[1])+'*'
                output=io.BytesIO()
                layer.image.save(output,'PNG')
                img_str = base64.b64encode(output.getvalue())
                bytes=img_str.decode()
                line +=bytes
            target.write(line)
        target.close()

    #this method exports a map to a .png image for use in other applications.
    def export_to_png(self,file):
        artist = Masterdrawer(self.layerlist)
        currentmap = artist.drawstack()
        currentmap.save(file)
        currentmap.close()

    # adds a layer to the stack using the currently active tile set
    def addlayer(self):
        self.layerlist.new_layer(self.get_active_tile_set())

    # code that creates a transparent image layer, if a file is specified it will instead create an image layer using the specified image file
    def add_img_layer(self,file=""):
        if file !="":
            try:
                img = Image.open(file)
                if img.size!= (self.layerlist.get_width(), self.layerlist.get_height()):
                    print("Image incorrect size")
                    img.close()
                    return "Invalid image file"
                else: print ("File loaded:",img.size,(self.layerlist.get_width(), self.layerlist.get_height()))
            except:
                print ("File not found")
        else:
            img=  Image.new('RGBA', (self.layerlist.get_width(), self.layerlist.get_height()), (255, 0, 0, 0))
        name = " (IO)layer "+ str(self.active_layer+1)
        self.layerlist.add_image_layer(name,img)

    # getter for the active tile set
    def get_active_tile_set(self):
        return self.active_tile_set

    # getter that reports the size of the current map as a tupple
    def get_map_size(self):
        return (self.layerlist.get_width(),self.layerlist.get_height())
    # undo function that uses an array of stored layerstacks which is updated when a change is made. Steps the current layerstack back to the previous stack in the array.
    def undo(self):
        if self.undo_redo_position > 0:
            self.undo_redo_position +=-1
            self.active_layer=1
            print ("loading from",self.undo_redo_position)
            self.layerlist=copy.deepcopy(self.undo_redo_array[self.undo_redo_position])

    #method that allows the free placement of tiles on image only layers accetps a tupple containing the co-ordinates of a click.
    def drawonimageonly(self,touch):
        pencil = Image.open(self.working_dir+"imgstore/activetiledisplay.png")
        paper = self.layerlist.get_layer(self.active_layer).image
        x = int(touch[0])
        y= int(abs(touch[1]-self.layerlist.get_height()))
        xadjust = pencil.size[0]//2
        yadjust = pencil.size[1]//2
        x= x-xadjust
        y= y-yadjust
        paper.paste(pencil,(x,y))
        pencil.close()

    #simple method that gives the active layer object's type - allows the frontend to tell if a layer is an image layer or a normal grid layer.
    def active_layer_type(self):
        return isinstance(self.layerlist.get_layer(self.active_layer),Layer)

    # the redo function - same as undo but steps forward through the unde_redo_array.
    def redo(self):
        if self.undo_redo_position<len(self.undo_redo_array)-1:
            self.active_layer=1
            self.undo_redo_position+=1
            print ("loading from",self.undo_redo_position)
            self.layerlist=copy.deepcopy(self.undo_redo_array[self.undo_redo_position])
