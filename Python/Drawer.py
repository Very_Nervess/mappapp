# a drawer class that takes layer . It draws a blank transaprent PNG and then populates it with the tiles specified in the layer by comparing their targets to the tilemap.
from Layer import *
from Tilemap import *
from Tile import *
class Drawer:
    def __init__(self,layer):
        self.canvas=layer
        self.tilemap=layer.tileset

    def drawmap(self):
        #draws a blank image
        blank = Image.new('RGBA', (self.canvas.get_width(), self.canvas.get_height()), (255, 0, 0, 0))
        # steps through the canvas and extracts each tile, then uses that tiles targets to crop an image from the tile map
        for i in range(self.canvas.get_numberofcolumns()):
            for j in range (self.canvas.get_numberofrows()):
                tiletemp = self.canvas.get_tile(i,j)
                #code to paste each tile's target image onto the blank base
                tileimage = self.tilemap.get_tileimage(tiletemp)
                blank.paste(tileimage,(i*self.tilemap.get_gridres(),j*self.tilemap.get_gridres()))
                tileimage.close()
        return blank
        blank.close()
