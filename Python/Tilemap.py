# class for the tilemap, the template from which all tiles are copied. Contains the target file and a grid resolution that seperates out individial tiles. Also referred to as the Tileset
from PIL import Image

class Tilemap:
    def __init__(self,targetfile,gridresolution=64):
        self.targetfile = targetfile
        self.gridresolution = gridresolution
        self.tileset = Image.open(targetfile)

#method to extract a specific sub image from the tile map.
    def get_tileimage(self,tile):
        col = tile.get_targetcol()
        row = tile.get_targetrow()
        left = col*self.get_gridres()
        upper = row*self.get_gridres()
        right = left+self.get_gridres()
        lower = upper+self.get_gridres()
        box = (left,upper,right,lower)
        return self.tileset.crop(box)
#getters:
    def get_gridres(self):
        return self.gridresolution

    def get_targetfile(self):
        return self.targetfile

    def get_imagesize(self):
        return self.tileset.size
# close method to close the image object opened with the tilset.
    def tilemapimageclose(self):
        self.tileset.close()
