#class that builds a list, or stack of layers. The initializing layer controls the attributes of other layers added. The stack can include imageonly layers
from Layer import *
from Img_Only_layer import *
class Layerstack:
    def __init__(self,layer):
        self.stack = []
        self.stack.append(layer)
# adds a layer to the list
    def add_layer(self,layer):
        self.stack.append(layer)
# adds an image only layer to the stack
    def add_image_layer(self,name,image):
        self.stack.append(Imageonly(name,image))

# creates a new blank layer and adds it to the top of the list
    def new_layer(self,tileset):
        newlayer = Layer("Layer "+str(len(self.stack)),self.stack[0].get_width(),self.stack[0].get_height(),tileset,self.stack[0].get_gridres())
        self.stack.append(newlayer)

# deletes a layer based on its position in the list. If it is an image only layer it closes the image object associated with that layer before deleting it.
    def del_layer(self,index):
        if isinstance(self.stack[index],Imageonly):
            self.stack[index].image.close()
        del self.stack[index]

# swaps two layers that already exist in the list
    def swap_layers(self,layer1,newposition):
        temp = self.stack[newposition]
        oldslot = self.stack.index(layer1)
        self.stack[newposition] = layer1
        self.stack[oldslot] = temp

#setter for layer name:
    def set_name(self,position,name):
        self.stack[position].name=name

# method that updates the size of all of the layers in the stack according to a given width and  height
    def set_map_size(self,width,height):
        for i in range(len(self.stack)):
            self.stack[i].set_size(width,height)

# extracts a single layer from the list based on its position in the list
    def get_layer(self,layerindex):
        return self.stack[layerindex]

# gets the list of layers
    def get_layerlist(self):
        return self.stack

# method that updates a specific tile in a specific layer to target a new target tile
    def update_tile_in_layer(self,layerindex,tilex,tiley,targetx,targety):
        self.stack[layerindex].set_tile(tilex,tiley,targetx,targety)

# getters that extract dimension information from the bottom layer in the stack
    def get_width(self):
        return self.stack[0].get_width()
    def get_height(self):
        return self.stack[0].get_height()
    def get_gridres(self):
        return self.stack[0].get_gridres()
    def get_stack_size(self):
        return len(self.stack)
