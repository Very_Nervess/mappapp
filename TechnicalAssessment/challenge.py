#I have made the following assumptions:
#1: The given file is in the same directory as the .py file
#2: by output the question means write/ update two new files to disk (rather than return them)
#3: an IP is considered valid if: It consists of 4 integers between 0 and 255(including 0,255) with each integer seperated by a single period.
#4: A valid IP will be considered invalid if it is one of the reserved addresses: 0.0.0.0, 255.255.255.255 or 127.0.0.1

import argparse

#this code handles the parsing of inputs from CMD - allows the script to be run in a single line as requested.
parser = argparse.ArgumentParser()
parser.add_argument('path', metavar='File Name (STR)', type=str)
args = parser.parse_args()

#this code opens the given (input) file and the two output files
original_file = open(args.path,"r")
validips = open("validIPs.txt","w")
invalidips= open("invalidIPs.txt","w")
# this code iterates over the original file, and checks each line to see if it is a valid IP
for line in original_file:
    line = line.replace("\n","")                                                   #the text is cleaned of next-line characters.
    digits = line.split(".")                                                       #the string is split on each period
    valid = True                                                                   #the IP is assumed to be valid until proven otherwise
    if len(digits)<4:                                                              #if the string contains less than 4 digits, the IP is considered false.
        valid=False
    else:
        for value in digits:                                                           #the program then checks each of the 4 digits in each IP
            if not(value.isnumeric()):
                valid = False                                                          #if any of the digits are non numeric the IP is considered false
            elif int(value) > 255 or int(value)<0:
                valid = False                                                          #if any of the digits are less than 0 or greater than 255 the IP is considered false. No error checking is required when casting here as the statement above filters for non-numeric characters.
    if valid==True and line !='0.0.0.0' and line!="255.255.255.255"and line!="127.0.0.1": #if th IP is still valid, and is not one of the 3 reserved IPs it is written to the validips file with each IP written on a new line
        validips.write(line+"\n")
    else:                                                                               #if it is invalid for any reason it is written to the invalidips file with each IP written on a new line
        invalidips.write(line+"\n")
# once the program has looped over the entire original file, all of the files are closed in memory.
original_file.close()
validips.close()
invalidips.close()
