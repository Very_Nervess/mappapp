# this code handles the user interface. As a disclaimer - its very messy. This has been my first time working with Kivy and UIs in general. I've learnt a lot in doing so and it works mostly, though a lot of it could be done much better.
from kivy.config import Config
from kivy.core.window import Window
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.textinput import TextInput
from kivy.uix.textinput import Image as Kiv_Image
from kivy.core.image import Image as CoreImage
from kivy.uix.scrollview import ScrollView
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.graphics import Color, Rectangle, Ellipse, Line, InstructionGroup
from kivy.effects.dampedscroll import DampedScrollEffect
from kivy.clock import Clock
from kivy.uix.filechooser import FileChooserIconView
from kivy.properties import ObjectProperty
from kivy.graphics.texture import Texture
from math import sqrt
import io
import os
from Backend1 import *
from PIL import Image

class MyFileChooser(FileChooserIconView):
    pass


# custome image class constructors that define the behavior of mouse clicks inside the displayed image, one for the tile set and one for the map.
class CreateScrollerImageTileset(Kiv_Image):
    # method defining behavior on mous click - imply updates the target tile based on the x,y position of the mouse touch.
    def on_touch_down(self, touch):
        self.parent.parent.backend.target_array=[]
        if self.collide_point(*touch.pos):
            touch.apply_transform_2d(self.to_local)
            if touch.button == "right":
                print (self.id)
                print("Right mouse clicked on {}".format(touch.pos))
                self.parent.parent.backend.set_active_tile(touch.pos)
                self.parent.parent.backend.drawactivetile()
                self.parent.parent.children[5].children[0].reload()
            elif touch.button == "left":
                print("Left mouse clicked on {}".format(touch.pos))
                self.parent.parent.backend.set_active_tile(touch.pos)
                self.parent.parent.backend.drawactivetile()
                self.parent.parent.children[5].children[0].reload()
                print(self.parent.parent.backend.active_tile)
            else:
                print(self.id)
            return True
        return super(CreateScrollerImageTileset, self).on_touch_down(touch)
    # method controlling click and drag behavior - this is used to update the cursor showing a scaling selection area as the mouse moves.
    def on_touch_move(self, touch):
        if self.parent.collide_point(*Window.mouse_pos):
            if touch.button=="right":
                self.children[0].background_color = [1,1,0,0.3]
                test = self.parent.to_local(*Window.mouse_pos)
                stored_res = self.children[1].size[0]
                box_start= (self.parent.parent.backend.active_tile.get_targetcol(),self.parent.parent.backend.active_tile.get_targetrow())
                box_stop = self.parent.parent.backend.coord_convert_tileset(test)
                x1 =box_start[0]
                x2 =box_stop[0]
                y1 =box_start[1]
                y2 =box_stop[1]
                xstart = min(x1,x2)
                ystart = min(y1,y2)
                xfin = max(x1,x2)
                yfin = max(y1,y2)
                width = max(1,(xfin-xstart)+1)*stored_res
                height = max(1,(yfin-ystart)+1)*stored_res
                self.children[0].size=(width,height)
                y_pos =self.parent.parent.backend.active_tile_set.get_imagesize()[1]-(ystart*stored_res)-height
                self.children[0].pos=(xstart*stored_res,y_pos)
    # method controlling events that occur on touch release  - this is used to resolve draggable selection areas. In this case it sets the active tile to a group of tiles as desccribed by the selection area.
    def on_touch_up(self,touch):
        self.children[0].background_color = [1,1,0,0]
        if touch.button== "left":
            pass
        elif touch.button =="right":
            box_start= (self.parent.parent.backend.active_tile.get_targetcol(),self.parent.parent.backend.active_tile.get_targetrow())
            box_stop = self.parent.parent.backend.coord_convert_tileset(touch.pos)
            x1 =box_start[0]
            x2 =box_stop[0]
            y1 =box_start[1]
            y2 =box_stop[1]
            xstart = min(x1,x2)
            ystart = min(y1,y2)
            xfin = max(x1,x2)
            yfin = max(y1,y2)
            for i in range(xstart,xfin+1):
                self.parent.parent.backend.target_array.append([])
                for j in range(ystart,yfin+1):
                    tup = (i,j)
                    self.parent.parent.backend.target_array[len(self.parent.parent.backend.target_array)-1].append(tup)
            self.parent.parent.backend.drawactivetile()
            self.parent.parent.children[5].children[0].reload()
            if len(self.parent.parent.backend.target_array) < 2 and len(self.parent.parent.backend.target_array[0]) < 2:
                self.parent.parent.backend.target_array =[]
        else:
            print(self.id)
            return True
        self.parent.parent.backend.clearselectionarray()
        return super(CreateScrollerImageTileset, self).on_touch_up(touch)
# custome image class constructors that define the behavior of mouse clicks inside the displayed image, one for the tile set and one for the map.
class CreateScrollerImageMap(Kiv_Image):

# on clikc behavior - checks to see what draw mode is active and then calls the appropriate method on a left click, on a right click the target tile's image is extracted for use as the active tile.
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            touch.grab(self)
            touch.apply_transform_2d(self.to_local)
            self.parent.parent.backend.selectionarray.append(self.parent.parent.backend.coord_convert_map(touch.pos))
            if touch.button == "right":
                # right click behavior is handled here - updates the active tile to the target of the selected tile.
                if self.parent.parent.backend.active_layer_type()==False:
                    return ""
                self.parent.parent.backend.target_array=[]
                print (self.id)
                print("Right mouse clicked on {}".format(touch))
                gridcoor=self.parent.parent.backend.coord_convert_map(touch.pos)
                tile = self.parent.parent.backend.layerlist.get_layer(self.parent.parent.backend.active_layer).get_tile(gridcoor[0],gridcoor[1])
                self.parent.parent.backend.active_tile=tile
                self.parent.parent.backend.drawactivetile()
                self.parent.parent.children[5].children[0].reload()
            elif touch.button == "left":
                # the left click behavior handles drawing on the map using the current active tile and the current active laer.
                if self.parent.parent.backend.active_layer_type()==False:
                    self.parent.parent.backend.drawonimageonly(touch.pos)
                    memorymap = CoreImage(self.parent.parent.backend.drawmap(),ext='png')
                    self.texture = memorymap.texture
                    return ""
                if self.parent.parent.backend.fillmode<0:
                    if not(self.parent.parent.backend.check_boundry_touch(touch.pos)):
                        if len(self.parent.parent.backend.target_array)>0: #this code checks if multiple tiles have been selected as a target, if so it draws them and breakso ut of the method.
                            self.parent.parent.backend.draw_array(self.parent.parent.backend.coord_convert_map(touch.pos))
                            memorymap = CoreImage(self.parent.parent.backend.drawmap(),ext='png') #the self.parent.parent is used to refer to the parent widget of this child, in this case the image refers to the scroll viewer which refers to the main window which as the backend code
                            self.texture = memorymap.texture
                            return "Target_array detected, drew array on click"
                        print("Left mouse clicked on {}".format(touch))
                        print("tile targeted:,",self.parent.parent.backend.coord_convert_map(touch.pos))
                        self.parent.parent.backend.update_target_tile(touch.pos)
                        memorymap = CoreImage(self.parent.parent.backend.drawmap(),ext='png') #the self.parent.parent is used to refer to the parent widget of this child, in this case the image refers to the scroll viewer which refers to the main window which as the backend code
                        self.texture = memorymap.texture #reloads the image with the updated tile
                    else: self.parent.parent.backend.selectionarray.append(self.parent.parent.backend.coord_convert_map(touch.pos))
                else:
                # code relating to the fill command goes here.
                    target = self.parent.parent.backend.coord_convert_map(touch.pos)
                    self.parent.parent.backend.fill_tool(target)
                    memorymap = CoreImage(self.parent.parent.backend.drawmap(),ext='png') #the self.parent.parent is used to refer to the parent widget of this child, in this case the image refers to the scroll viewer which refers to the main window which as the backend code
                    self.texture = memorymap.texture

            else:
                print(self.id)
            return True
        return super(CreateScrollerImageMap, self).on_touch_down(touch)
#this code handles click and drag touches. It also updates the cursor selection area display.
    def on_touch_move(self, touch):
        if self.parent.parent.backend.active_layer_type()==False:
            return ""
        if self.parent.parent.backend.fillmode>0:return""
        if self.parent.parent.backend.boxmode<0:
            if self.collide_point(*touch.pos):
                touch.apply_transform_2d(self.to_local)
                if touch.button == "right":
                    """this code used to update the active tile when the cursor was clicked and dragged over it, but as that function now selects a section of the map to tile it is not needed.
                    print (self.id)
                    print("Right mouse clicked on {}".format(touch))
                    gridcoor=self.parent.parent.backend.coord_convert_map(touch.pos)
                    tile = self.parent.parent.backend.layerlist.get_layer(self.parent.parent.backend.active_layer).get_tile(gridcoor[0],gridcoor[1])
                    self.parent.parent.backend.active_tile=tile
                    self.parent.parent.backend.drawactivetile()
                    self.parent.parent.children[5].children[0].reload()"""
                    if self.parent.collide_point(*Window.mouse_pos):
                        self.children[0].background_color = [1,1,0,0.3]
                        test = self.parent.to_local(*Window.mouse_pos)
                        stored_res = self.children[1].size[0]
                        box_start= self.parent.parent.backend.selectionarray[0]
                        box_stop = self.parent.parent.backend.coord_convert_map(self.parent.to_local(*Window.mouse_pos))
                        x1 =box_start[0]
                        x2 =box_stop[0]
                        y1 =box_start[1]
                        y2 =box_stop[1]
                        xstart = min(x1,x2)
                        ystart = min(y1,y2)
                        xfin = max(x1,x2)
                        yfin = max(y1,y2)
                        width = max(1,(xfin-xstart)+1)*stored_res
                        height = max(1,(yfin-ystart)+1)*stored_res
                        self.children[0].size=(width,height)
                        y_pos =self.parent.parent.backend.layerlist.get_height()-(ystart*stored_res)-height
                        self.children[0].pos=(xstart*stored_res,y_pos)

                elif touch.button == "left":
                    if not(self.parent.parent.backend.check_boundry_touch(touch.pos)):
                        gridco = self.parent.parent.backend.coord_convert_map(touch.pos)
                        x2 = gridco[0]
                        y2 = gridco[1]
                        x1 = self.parent.parent.backend.selectionarray[len(self.parent.parent.backend.selectionarray)-1][0]
                        y1 = self.parent.parent.backend.selectionarray[len(self.parent.parent.backend.selectionarray)-1][1]
                        distance = sqrt((x2-x1)**2+(y2-y1)**2)
                        if distance < 2 and not(gridco in self.parent.parent.backend.selectionarray):
                            self.parent.parent.backend.selectionarray.append(self.parent.parent.backend.coord_convert_map(touch.pos))
                            self.parent.parent.backend.update_target_tile_grid(gridco)
                            memorymap = CoreImage(self.parent.parent.backend.drawmap(),ext='png') #the self.parent.parent is used to refer to the parent widget of this child, in this case the image refers to the scroll viewer which refers to the main window which as the backend code
                            self.texture = memorymap.texture
                else:
                    print(self.id)
        else:
            if touch.button =="left":
                if self.parent.collide_point(*Window.mouse_pos):
                    self.children[0].background_color = [0,0,1,0.3]
                    test = self.parent.to_local(*Window.mouse_pos)
                    stored_res = self.children[1].size[0]
                    box_start= self.parent.parent.backend.selectionarray[0]
                    box_stop = self.parent.parent.backend.coord_convert_map(self.parent.to_local(*Window.mouse_pos))
                    x1 =box_start[0]
                    x2 =box_stop[0]
                    y1 =box_start[1]
                    y2 =box_stop[1]
                    xstart = min(x1,x2)
                    ystart = min(y1,y2)
                    xfin = max(x1,x2)
                    yfin = max(y1,y2)
                    width = max(1,(xfin-xstart)+1)*stored_res
                    height = max(1,(yfin-ystart)+1)*stored_res
                    self.children[0].size=(width,height)
                    y_pos =self.parent.parent.backend.layerlist.get_height()-(ystart*stored_res)-height
                    self.children[0].pos=(xstart*stored_res,y_pos)
            elif touch.button=="right":
                if self.parent.collide_point(*Window.mouse_pos):
                    self.children[0].background_color = [1,1,0,0.3]
                    test = self.parent.to_local(*Window.mouse_pos)
                    stored_res = self.children[1].size[0]
                    box_start= self.parent.parent.backend.selectionarray[0]
                    box_stop = self.parent.parent.backend.coord_convert_map(self.parent.to_local(*Window.mouse_pos))
                    x1 =box_start[0]
                    x2 =box_stop[0]
                    y1 =box_start[1]
                    y2 =box_stop[1]
                    xstart = min(x1,x2)
                    ystart = min(y1,y2)
                    xfin = max(x1,x2)
                    yfin = max(y1,y2)
                    width = max(1,(xfin-xstart)+1)*stored_res
                    height = max(1,(yfin-ystart)+1)*stored_res
                    self.children[0].size=(width,height)
                    y_pos =self.parent.parent.backend.layerlist.get_height()-(ystart*stored_res)-height
                    self.children[0].pos=(xstart*stored_res,y_pos)

#this code handles touch release behavior
    def on_touch_up(self,touch):
        if self.parent.parent.backend.active_layer_type()==False:
            return ""
        print("Click up called @",touch.pos,touch.button)
        self.children[0].background_color = [0,0,1,0]

        if touch.button== "left":
            if len(self.parent.parent.backend.selectionarray) <1:
                return "Array too small"
            if self.parent.parent.backend.boxmode>0: #checks to see if box draw mode is on
                if len(self.parent.parent.backend.selectionarray) ==1:
                    self.parent.parent.backend.selectionarray.append(self.parent.parent.backend.coord_convert_map(touch.pos)) #adds the mosition where the cursor was lifted from the set to the selection array
                box_start= self.parent.parent.backend.selectionarray[0]
                box_stop = self.parent.parent.backend.selectionarray[1]
                x1 =box_start[0]
                x2 =box_stop[0]
                y1 =box_start[1]
                y2 =box_stop[1]
                xstart = min(x1,x2)
                ystart = min(y1,y2)
                xfin = max(x1,x2)
                yfin = max(y1,y2)
                for i in range(xstart,xfin+1):
                    for j in range(ystart,yfin+1):
                        self.parent.parent.backend.update_target_tile_grid((i,j))
                memorymap = CoreImage(self.parent.parent.backend.drawmap(),ext='png') #the self.parent.parent is used to refer to the parent widget of this child, in this case the image refers to the scroll viewer which refers to the main window which as the backend code
                self.texture = memorymap.texture
                self.children[0].size=(self.parent.parent.backend.layerlist.get_gridres(),self.parent.parent.backend.layerlist.get_gridres())

        elif touch.button =="right":
            if len(self.parent.parent.backend.selectionarray)==0:return""
            box_start= self.parent.parent.backend.selectionarray[0]
            box_stop = self.parent.parent.backend.coord_convert_map(touch.pos)
            x1 =box_start[0]
            x2 =box_stop[0]
            y1 =box_start[1]
            y2 =box_stop[1]
            xstart = min(x1,x2)
            ystart = min(y1,y2)
            xfin = max(x1,x2)
            yfin = max(y1,y2)
            layer = self.parent.parent.backend.layerlist.get_layer(self.parent.parent.backend.active_layer)
            for i in range(xstart,xfin+1):
                self.parent.parent.backend.target_array.append([])
                for j in range(ystart,yfin+1):
                    tile = layer.get_tile(i,j)
                    tup = (tile.get_targetcol(),tile.get_targetrow())
                    self.parent.parent.backend.target_array[len(self.parent.parent.backend.target_array)-1].append(tup)
            if len(self.parent.parent.backend.target_array) <2 and len(self.parent.parent.backend.target_array[0])  < 2:
                self.parent.parent.backend.target_array =[]
            self.parent.parent.backend.drawactivetile()
            self.parent.parent.children[5].children[0].reload()

        else:
            print(self.id)
            return True
        self.parent.parent.backend.clearselectionarray()
        return super(CreateScrollerImageMap, self).on_touch_up(touch)

# This class describes the main GUI window.
class MainScreen(GridLayout):

    def __init__(self, **kwargs):
        super(MainScreen, self).__init__(**kwargs)
        # code to specify the background and padding values for the main window.

        self. padding=20
        self.spacing = 10
        with self.canvas.before:
            Color(1,1,1,0.2)
            self.rect = Rectangle(pos=self.pos,size=(200,200))#size=self.size)
            self.bind(size=self.upd, pos=self.upd)

        # creates the backend that handles all processing
        self.backend=Backend()
        print ("directory,",self.backend.working_dir)
        print (sys.argv[0])
        # edits values of the main window grid that holds all sub widgets
        self.cols = 3
        self.rows = 2
        #code for the layermap menue goes here: A grid with 2 columns, one containing the active tile display and the other the dropdown menue for tilset selection
        tilemapmenue = GridLayout(size_hint=(None,None),size = (300,100),padding=20,pos_hint=(None,1))
        tilemapmenue.cols=3
        tilemapmenue.padding = 0
        tilemapmenue.spacing = 0
        with self.canvas.before:
            Color(0.5,0.5,0.5,1)
            tilemapmenue.recttmm = Rectangle(pos=tilemapmenue.pos,size=tilemapmenue.size)

        #code for the tilesetdrop down menu goes here:
        tilsetselectgrid= GridLayout(rows=2)
        tilesetdropdown=DropDown()
        availablesets=os.listdir(self.backend.working_dir+'imgstore/tilesets')
        def handle_Tileset_select(instance):
            file = self.backend.working_dir+"imgstore/tilesets/"+instance.text+".png"
            if file != self.backend.get_active_tile_set().get_targetfile():
                self.backend.set_active_tileset(file)
                if self.backend.active_layer_type():
                    self.backend.addlayer()
                    self.backend.set_active_layer(self.backend.active_layer+1)
                imw.source=self.backend.get_active_tile_set().get_targetfile()
                imw.size=self.backend.get_active_tile_set().get_imagesize()
                populatelayerlist()
                imw.reload()
                self.backend.drawactivetile()
                imw3.reload()
                currentset.text=self.backend.active_tile_set.get_targetfile().split('/')[-1]
                tilesetdropdown.dismiss()
            else:tilesetdropdown.dismiss()
        def open_tile_set_dropdown(instance):
            tilesetdropdown.open(instance)

        for i in availablesets:
            setname = i[:len(i)-4]
            setbtn = Button(text=setname,size_hint_y=None, height=44)
            setbtn.bind(on_press=handle_Tileset_select)
            tilesetdropdown.add_widget(setbtn)
        tilesetdropdownbtn = Button(text="Select",size_hint=(1,None),size=(150,30),pos_hint=(0,1))
        tilesetdropdownbtn.bind(on_release=open_tile_set_dropdown)
        currentset=Label(text=self.backend.active_tile_set.get_targetfile().split('/')[-1])
        tilsetselectgrid.add_widget(currentset)
        tilsetselectgrid.add_widget(tilesetdropdownbtn)
        tilemapmenue.add_widget(tilsetselectgrid)
        tilemapmenue.add_widget(Label(text="Active\nTile    :",font_size='18sp',size_hint=(0.5,0.5)))
        imw3 = Kiv_Image(source=self.backend.working_dir+"imgstore/activetiledisplay.png")
        imw3.pos_hint=(1,1)
        tilemapmenue.add_widget(imw3)
        # adds the layermap menue, map menue and a blank widget to the top of the window
        self.add_widget(tilemapmenue)
        # code for the map control menue goes here



        """The Following code describes the mapfile menue (save,load,export,new,resize)"""
        self.newh = self.backend.layerlist.get_width()
        self.neww = self.backend.layerlist.get_height()
        #Function to control the buttons in the map control menue
        def mapcontrolbuttons(instance):
            if instance.text=="Load":
                """The following code describes the widget used to load files"""
                filebrowser = MyFileChooser()
                filetext = TextInput(text="File.txt",multiline=False,size=(345,30),size_hint=(None,None))
                filebrowser.add_widget(filetext)
                filebrowser.path=self.backend.working_dir+"Saved maps"
                filetext.text=filebrowser.path
                filebrowsercancel = Button(text="Cancel",size=(60,30),size_hint=(None,None),pos=(415,0))
                filebrowserokay = Button(text="Load",size=(60,30),size_hint=(None,None),pos=(350,0))
                filebrowser.add_widget(filebrowsercancel)
                filebrowser.add_widget(filebrowserokay)
                def fileokay(instance):
                    try:
                        self.backend.load_from_file(filebrowser.selection[0])
                        memorymap = CoreImage(self.backend.drawmap(),ext='png')
                        self.backend.set_active_layer(self.backend.layerlist.get_stack_size()-1)
                        imw2.size=(self.backend.layerlist.get_width(),self.backend.layerlist.get_height())
                        imw2.texture=memorymap.texture
                        populatelayerlist()
                        pop.dismiss()
                    except Exception as p:
                        print(p)
                        error=Popup(
                        title=" An error Occured:",
                        content=(Label(text="Invalid File")),
                        size_hint=(None,None),
                        size=(400,200),
                        auto_dismiss = True
                        )
                        error.open()
                def filecancel(instance):
                    pop.dismiss()
                def updatefiletextondir(instance,path):
                    filetext.text=filebrowser.path+"\\"
                def updatefiletextonselec(instance,selection,touch):
                    try:
                        path =""
                        for i in selection:
                            path = i
                            filetext.text=(path)
                            self.backend.load_from_file(filetext.text)
                            memorymap = CoreImage(self.backend.drawmap(),ext='png')
                            self.backend.set_active_layer(self.backend.layerlist.get_stack_size()-1)
                            imw2.size=(self.backend.layerlist.get_width(),self.backend.layerlist.get_height())
                            imw2.texture=memorymap.texture
                            populatelayerlist()
                            pop.dismiss()
                    except Exception as e:
                        error=Popup(
                        title=" An error Occured:",
                        content=(Label(text="Invalid File")),
                        size_hint=(None,None),
                        size=(400,200),
                        auto_dismiss = True
                        )
                        error.open()
                        print (e)
                filebrowser.bind(on_submit=updatefiletextonselec,path=updatefiletextondir)
                filebrowsercancel.bind(on_press=filecancel)
                filebrowserokay.bind(on_press=fileokay)
                pop=Popup(
                title="Load mapstack. !! UNSAVED PROGRESS WILL BE LOST!!",
                content=(filebrowser),
                size_hint=(None,None),
                size=(500,500),
                auto_dismiss = True
                )
                pop.open()
            elif instance.text =="New":
                """This section handles creating a new map according to the users specifications"""
                # big and complicated function that calls a pop-up with input fields for width and height
                width = self.backend.layerlist.get_width()
                height = self.backend.layerlist.get_height()
                # the next two methods store values from the text fields on the popup. They have data validation and deploy another notification popup if invalid data is entered.
                def storewidth(instance,value):
                    if instance.text !="":
                        self.neww = int(instance.text)*self.backend.layerlist.get_gridres()
                def storeheight(instance,value):
                    if instance.text !="":
                        self.newh = int(instance.text)*self.backend.layerlist.get_gridres()
                # this function is called when the 'okay' button is pressed and it takes the new values stored from from the input fields and uses them to update the map's size.
                def resolvenew(instance):
                    self.backend.create_drawspace(64,64)
                    self.backend.layerlist.set_map_size(self.neww,self.newh)
                    memorymap = CoreImage(self.backend.drawmap(),ext='png')
                    imw2.size=(self.neww,self.newh) #the map must be redrawn and then the element must be resized to the new size.
                    imw2.texture=memorymap.texture
                    pop.dismiss()
                    self.backend.set_active_layer(1)
                    populatelayerlist()
                def resolvenew2(instance):
                    pop.dismiss() #on the canel button being pressed the pop-up is dismissed and the size is not updated.
                # this code generates all the buttons that are housed in the popup window. They must be housed in an outer grid.
                inputs=GridLayout(rows=3,cols=2)
                input1=TextInput(text=str(int(width/self.backend.layerlist.get_gridres())))
                input1.multiline=False
                input1.bind(text=storewidth)
                input2=TextInput(text=str(int(height/self.backend.layerlist.get_gridres())))
                input2.multiline=False
                input2.bind(text=storeheight)
                inputs.add_widget(Label(text="Grid Width"))
                inputs.add_widget(Label(text="Grid Height"))
                inputs.add_widget(input1)
                inputs.add_widget(input2)
                okaybtn=Button(text="Create New")
                cancelbtn=Button(text="Cancel")
                okaybtn.bind(on_press=resolvenew)
                cancelbtn.bind(on_press=resolvenew2)
                inputs.add_widget(cancelbtn)
                inputs.add_widget(okaybtn)
                # this code creates the popup
                pop=Popup(
                title=" !! WARNING, UNSAVED PROGRESS WILL BE LOST !!",
                content=(inputs),
                size_hint=(None,None),
                size=(400,200),
                auto_dismiss = True
                )
                pop.open()

            elif instance.text =="Resize":
                """This section handles resizing the current map"""
                # big and complicated function that calls a pop-up with input fields for width and height
                width = self.backend.layerlist.get_width()
                height = self.backend.layerlist.get_height()
                # the next two methods store values from the text fields on the popup. They have data validation and deploy another notification popup if invalid data is entered.
                def storewidth(instance,value):
                    if instance.text !="":
                        self.neww = int(instance.text)*self.backend.layerlist.get_gridres()
                def storeheight(instance,value):
                    if instance.text !="":
                        self.newh = int(instance.text)*self.backend.layerlist.get_gridres()
                # this function is called when the 'okay' button is pressed and it takes the new values stored from from the input fields and uses them to update the map's size.
                def resolveresize(instance):
                    self.backend.layerlist.set_map_size(self.neww,self.newh)
                    memorymap = CoreImage(self.backend.drawmap(),ext='png')
                    imw2.size=(self.neww,self.newh) #the map must be redrawn and then the element must be resized to the new size.
                    imw2.texture=memorymap.texture
                    pop.dismiss()
                def resolveresize2(instance):
                    pop.dismiss() #on the canel button being pressed the pop-up is dismissed and the size is not updated.
                # this code generates all the buttons that are housed in the popup window. They must be housed in an outer grid.
                inputs=GridLayout(rows=3,cols=2)
                input1=TextInput(text=str(int(width/self.backend.layerlist.get_gridres())))
                input1.multiline=False
                input1.bind(text=storewidth)
                input2=TextInput(text=str(int(height/self.backend.layerlist.get_gridres())))
                input2.multiline=False
                input2.bind(text=storeheight)
                inputs.add_widget(Label(text="Grid Width"))
                inputs.add_widget(Label(text="Grid Height"))
                inputs.add_widget(input1)
                inputs.add_widget(input2)
                okaybtn=Button(text="Okay")
                cancelbtn=Button(text="Cancel")
                okaybtn.bind(on_press=resolveresize)
                cancelbtn.bind(on_press=resolveresize2)
                inputs.add_widget(cancelbtn)
                inputs.add_widget(okaybtn)
                # this code creates the popup
                pop=Popup(
                title="Change Map Size:",
                content=(inputs),
                size_hint=(None,None),
                size=(400,200),
                auto_dismiss = True
                )
                pop.open()
            elif instance.text =="Save":
                """The following code describes the widget used to save files"""
                filebrowser = MyFileChooser()
                filetext = TextInput(text="File.txt",multiline=False,size=(345,30),size_hint=(None,None))
                filebrowser.add_widget(filetext)
                filetext.text=filebrowser.path+"\\Untitled.text"
                filebrowsercancel = Button(text="Cancel",size=(60,30),size_hint=(None,None),pos=(415,0))
                filebrowserokay = Button(text="Save",size=(60,30),size_hint=(None,None),pos=(350,0))
                filebrowser.add_widget(filebrowsercancel)
                filebrowser.add_widget(filebrowserokay)
                def fileokay(instance):
                    try:
                        self.backend.save_to_file(filetext.text)
                        pop.dismiss()
                    except Exception as e:
                        print(e)
                        error=Popup(
                        title=" An error Occured:",
                        content=(Label(text="Invalid File")),
                        size_hint=(None,None),
                        size=(400,200),
                        auto_dismiss = True
                        )
                        error.open()
                def filecancel(instance):
                    pop.dismiss()
                def updatefiletextondir(instance,path):
                    filetext.text=filebrowser.path+"\\Untitled.text"
                def updatefiletextonselec(instance,selection,touch):
                    path =""
                    for i in selection:
                        path = i
                        filetext.text=(path)
                    filebrowserokay.background_normal =""
                    filebrowserokay.background_color=(0.7,0,0,1)
                    filebrowserokay.color=(1,1,1,1)
                    filebrowserokay.text="Replace"
                filebrowser.bind(on_submit=updatefiletextonselec,path=updatefiletextondir)
                filebrowsercancel.bind(on_press=filecancel)
                filebrowserokay.bind(on_press=fileokay)
                def check_for_overwrite(instance,value):
                    checker = False
                    for i in filebrowser.files:
                        if i == instance.text:
                            checker = True
                    if checker:
                        filebrowserokay.background_normal =""
                        filebrowserokay.background_color=(0.7,0,0,1)
                        filebrowserokay.color=(1,1,1,1)
                        filebrowserokay.text="Replace"
                filetext.bind(text=check_for_overwrite)
                pop=Popup(
                title="Save Mapstack: If an existing file is selected it will be overwritten!",
                content=(filebrowser),
                size_hint=(None,None),
                size=(500,500),
                auto_dismiss = True
                )
                pop.open()
            elif instance.text =="Export":
                """The following code describes the widget used to Export files to png"""
                filebrowser = MyFileChooser()
                filetext = TextInput(text="File.txt",multiline=False,size=(345,30),size_hint=(None,None))
                filebrowser.add_widget(filetext)
                filetext.text=filebrowser.path+"\\Untitled.png"
                filebrowsercancel = Button(text="Cancel",size=(60,30),size_hint=(None,None),pos=(415,0))
                filebrowserokay = Button(text="Save",size=(60,30),size_hint=(None,None),pos=(350,0))
                filebrowser.add_widget(filebrowsercancel)
                filebrowser.add_widget(filebrowserokay)
                def fileokay(instance):
                    try:
                        self.backend.export_to_png(filetext.text)
                        pop.dismiss()
                    except:
                        error=Popup(
                        title=" An error Occured:",
                        content=(Label(text="Invalid File")),
                        size_hint=(None,None),
                        size=(400,200),
                        auto_dismiss = True
                        )
                        error.open()
                def filecancel(instance):
                    pop.dismiss()
                def updatefiletextondir(instance,path):
                    filetext.text=filebrowser.path+"\\Untitled.png"
                def updatefiletextonselec(instance,selection,touch):
                    path =""
                    for i in selection:
                        path = i
                        filetext.text=(path)
                    filebrowserokay.background_normal =""
                    filebrowserokay.background_color=(0.7,0,0,1)
                    filebrowserokay.color=(1,1,1,1)
                    filebrowserokay.text="Replace"
                filebrowser.bind(on_submit=updatefiletextonselec,path=updatefiletextondir)
                filebrowsercancel.bind(on_press=filecancel)
                filebrowserokay.bind(on_press=fileokay)
                def check_for_overwrite(instance,value):
                    checker = False
                    for i in filebrowser.files:
                        if i == instance.text:
                            checker = True
                    if checker:
                        filebrowserokay.background_normal =""
                        filebrowserokay.background_color=(0.7,0,0,1)
                        filebrowserokay.color=(1,1,1,1)
                        filebrowserokay.text="Replace"
                filetext.bind(text=check_for_overwrite)
                pop=Popup(
                title="Save Mapstack: If an existing file is selected it will be overwritten!",
                content=(filebrowser),
                size_hint=(None,None),
                size=(500,500),
                auto_dismiss = True
                )
                pop.open()
        #this code creates the buttons for all the buttons in the map-control panel.
        mapcontrol = GridLayout(cols = 5,size=(100,100),size_hint=(1,None),padding = 20,spacing = 5)
        loadbtn = Button(text="Load")
        newbtn = Button(text="New")
        resizebtn = Button(text="Resize")
        savebtn = Button(text="Save")
        exportbtn = Button(text="Export")
        loadbtn.bind(on_press=mapcontrolbuttons)
        newbtn.bind(on_press=mapcontrolbuttons)
        resizebtn.bind(on_press=mapcontrolbuttons)
        savebtn.bind(on_press=mapcontrolbuttons)
        exportbtn.bind(on_press=mapcontrolbuttons)
        mapcontrol.add_widget(loadbtn)
        mapcontrol.add_widget(newbtn)
        mapcontrol.add_widget(resizebtn)
        mapcontrol.add_widget(savebtn)
        mapcontrol.add_widget(exportbtn)
        self.add_widget(mapcontrol)
        self.add_widget(Label(text="",size=(200,100),size_hint=(None,None)))

        #code to display the Tile map element
        # method that updates the positions of the images in the scroll viewers when the window resizes
        def scrollupdater(instance,value):
            imw.pos_hint=(0,1)
            imw2.pos_hint=(0,1)



        tilemapviewer = ScrollView(
            size_hint=(None,1),
            size=(300,300),
            effect_cls=SafeScrollEffect #this line calls a custom class which is supposed to fix the recursion depth error
            ) # a scrolling viewer with a fixed width of 300 px, the height will scale with the window.
        tilemapviewer.scroll_type = ['bars'] #changes the scroll mode to use scroll bars
        tilemapviewer.bar_width = 16 #makes the bars larger and thus easier to use
        imw=CreateScrollerImageTileset(source=self.backend.get_active_tile_set().get_targetfile(),size_hint = (None,None),size =self.backend.get_active_tile_set().get_imagesize(),pos=(0,0)) #gets the tile map image from the backend
        tilemapviewer.add_widget(imw) # add the image to the scroll viwer.
        self.add_widget(tilemapviewer) #add the bracket to the base layout.

        #code to display the map

        #shell2 = FloatLayout() #a bracket to control the size and position of the image within the grid.
        mapviewer = ScrollView(
            size_hint=(1,1),
            effect_cls=SafeScrollEffect #this line calls a custom class which is supposed to fix the recursion depth error
            ) # a scrolling viewer with no restrictions on size
        mapviewer.scroll_type = ['bars'] #changes the scroll mode to use scroll bars
        mapviewer.bar_width = 16 #makes the bars larger and thus easier to use
        memorymap = CoreImage(self.backend.drawmap(),ext='png')
        imw2=CreateScrollerImageMap(texture=memorymap.texture,size_hint = (None,None),size =self.backend.get_map_size(),pos=(0,0)) #gets the map image
        mapviewer.add_widget(imw2) # add the image to the scroll viwer.
        mapcursor = Button(text="",size=(self.backend.layerlist.get_gridres(),self.backend.layerlist.get_gridres()))
        tilecursor = Button(text="",size=(self.backend.layerlist.get_gridres(),self.backend.layerlist.get_gridres()))
        tilecursor2 = Button(text="",size=(self.backend.layerlist.get_gridres(),self.backend.layerlist.get_gridres()))
        mapcursor2 = Button(text="",size=(mapcursor.size))
        mapcursor2.background_color = [0,0,1,0]
        tilecursor2.background_color = [0,0,1,0]
        imw2.add_widget(mapcursor)
        imw2.add_widget(mapcursor2)
        imw.add_widget(tilecursor)
        imw.add_widget(tilecursor2)
        self.add_widget(mapviewer) #add the bracket to the base layout.
        """This method listens for mouse movement over the two grid regions (tilset and map) and then draws a sqaure over the selection grid cell indicating what
        cell will be selected with a click..."""
        def reading(self, pos):
            if mapviewer.collide_point(*pos):
                tilecursor.background_color=[0,0,0,0]
                mapcursor.background_color = [0,0,1,0.3]
                mapcursor.size=mapviewer.parent.backend.getactivetilsesize()
                posl = mapviewer.to_local(*pos)
                res = mapviewer.parent.backend.layerlist.get_gridres()
                boundry = False
                touchx = posl[0]
                touchy = posl[1]
                xbound = touchx % res
                ybound = touchy % res
                if res-res//8< xbound or xbound < res//8 or res-res//8< xbound or xbound < res//8:
                    boundry = True
                if boundry==False and mapviewer.parent.backend.active_layer_type():
                    poslx = (posl[0]//res)*res
                    posly = (posl[1]//res)*res - 1*(mapcursor.size[1]-res)
                    posl = (poslx,posly)
                    mapcursor.pos=posl
                elif mapviewer.parent.backend.active_layer_type()==False:
                    xadjust = mapviewer.parent.backend.getactivetilsesize()[0]/2
                    yadjust = mapviewer.parent.backend.getactivetilsesize()[1]/2
                    x = posl[0]-xadjust
                    y = posl[1]-yadjust
                    mapcursor.pos=(x,y)
            elif tilemapviewer.collide_point(*pos):
                mapcursor.background_color=[0,0,0,0]
                tilecursor.background_color = [0,0,1,0.3]
                posl = tilemapviewer.to_local(*pos)
                poslx = (posl[0]//mapcursor.size[0])*mapcursor.size[0]
                posly = (posl[1]//mapcursor.size[0])*mapcursor.size[0]
                posl = (poslx,posly)
                tilecursor.pos=posl
            else:
                mapcursor.background_color=[0,0,0,0]
                tilecursor.background_color=[0,0,0,0]

        Window.bind(mouse_pos=reading)

        mapviewer.bind(pos=scrollupdater,size= scrollupdater)
        tilemapviewer.bind(pos=scrollupdater,size= scrollupdater)



        #code to display the tool panel and layer menue:
        #creates a grid to hold the two panels
        col3 = GridLayout()

        col3.rows=3
        col3.size_hint =(None,1)
        col3.size = (200,200)
        col3.pos_hint=(1,1)
        # creates the tool panel section
        toolpanel = GridLayout()

        toolpanel.size_hint=(None,None)
        toolpanel.size=(200,200)
        toolpanel.pos = (self.size)
        # updater method allowing bind function to reposition/ resize objects when the window changes. keeps the tool panel in the top, right corner.
        def toolpanelupdater(instance,value):
            #toolpanel.pos = (self.width-220,self.height-220)
            toolpanel.rect2.pos=toolpanel.pos
            toolpanel.rect2.size=toolpanel.size
            layermenue.rect3.pos=layermenue.pos
            layermenue.rect3.size=layermenue.size
            tilemapmenue.recttmm.size=tilemapmenue.size
            tilemapmenue.recttmm.pos=tilemapmenue.pos
        # command to draw and color a background for the tool panel
        with self.canvas.before:
            Color(0,0,1,1)
            toolpanel.rect2 = Rectangle(pos=toolpanel.pos,size=(200,200))

        # section where buttons are added to tool panel
        toolpanel.rows =3
        toolpanel.cols =3
        """Code for the tool panel buttons goes here"""
        #square draw toggle:
        def togglesqrdraw(instance):
            self.backend.boxmode *=-1
            self.backend.fillmode=-1
            fillbtn.background_color=(0.3,0.3,0.3,1)
            fillbtn.color=(1,1,1,1)
            if self.backend.boxmode>0:
                sqrdrawbtn.background_color=(0.8,0.8,0.8,1)
                sqrdrawbtn.color=(0,0,0,1)
            if self.backend.boxmode<0:
                sqrdrawbtn.background_color=(0.3,0.3,0.3,1)
                sqrdrawbtn.color=(1,1,1,1)
        sqrdrawbtn = Button(text="Square")
        sqrdrawbtn.background_normal =""
        sqrdrawbtn.background_color=(0.3,0.3,0.3,1)
        sqrdrawbtn.bind(on_press=togglesqrdraw)
        toolpanel.add_widget(sqrdrawbtn)
        #Toggle grid:
        def togglegrid(instance):
            if self.backend.grid==False:
                self.backend.grid = True
                gridbtn.background_color=(0.8,0.8,0.8,1)
                gridbtn.color=(0,0,0,1)
            elif self.backend.grid==True:
                self.backend.grid = False
                gridbtn.background_color=(0.3,0.3,0.3,1)
                gridbtn.color=(1,1,1,1)
            memorymap = CoreImage(self.backend.drawmap(),ext='png')
            imw2.texture = memorymap.texture
        gridbtn = Button(text="Grid?")
        gridbtn.background_normal =""
        gridbtn.background_color=(0.8,0.8,0.8,1)
        gridbtn.bind(on_press=togglegrid)
        toolpanel.add_widget(gridbtn)
        # toggle fill bucket mode:
        def togglefill(instance):
            sqrdrawbtn.background_color=(0.3,0.3,0.3,1)
            sqrdrawbtn.color=(1,1,1,1)
            self.backend.fillmode *=-1
            self.backend.boxmode=-1
            if self.backend.fillmode>0:
                fillbtn.background_color=(0.8,0.8,0.8,1)
                fillbtn.color=(0,0,0,1)
            if self.backend.fillmode<0:
                fillbtn.background_color=(0.3,0.3,0.3,1)
                fillbtn.color=(1,1,1,1)
        fillbtn = Button(text="Fill")
        fillbtn.background_normal =""
        fillbtn.background_color=(0.3,0.3,0.3,1)
        fillbtn.bind(on_press=togglefill)
        toolpanel.add_widget(fillbtn)
        undobtn = Button(text="Undo")
        undobtn.background_color=(0.3,0.3,0.3,1)
        undobtn.background_normal =""
        redobtn = Button(text="Redo")
        redobtn.background_color=(0.3,0.3,0.3,1)
        redobtn.background_normal =""
        def redocall(instance):
            self.backend.redo()
            print(self.backend.undo_redo_position,"unrepos")
            memorymap = CoreImage(self.backend.drawmap(True),ext='png')
            imw2.size=(self.backend.layerlist.get_width(),self.backend.layerlist.get_height())
            imw2.texture=memorymap.texture
            populatelayerlist()
        def undocall(instance):
            self.backend.undo()
            print(self.backend.undo_redo_position,"unrepos")
            memorymap = CoreImage(self.backend.drawmap(True),ext='png')
            print (self.backend.layerlist.get_width())
            imw2.size=(self.backend.layerlist.get_width(),self.backend.layerlist.get_height())
            imw2.texture=memorymap.texture
            populatelayerlist()
        redobtn.bind(on_press=redocall)
        undobtn.bind(on_press=undocall)
        toolpanel.add_widget(undobtn)
        toolpanel.add_widget(redobtn)
        toolpanel.add_widget(Label(text=""))

        imgbtn = Button(text="import\nimage",background_normal="",background_color=(0.3,0.3,0.3,1))
        def importimg(instance):
            self.backend.add_img_layer(self.backend.working_dir+"Saved maps\maptest.png")
            self.backend.active_layer+=1
            memorymap = CoreImage(self.backend.drawmap(True),ext='png')
            imw2.texture=memorymap.texture
            populatelayerlist()
        imgbtn.bind(on_press=importimg)
        toolpanel.add_widget(imgbtn)

        def freedraw(instance):
            self.backend.add_img_layer()
            self.backend.active_layer+=1
            populatelayerlist()

        freedrawbtn= Button(text="Create\nfreedraw",background_normal="",background_color=(0.3,0.3,0.3,1))
        freedrawbtn.bind(on_press=freedraw)
        toolpanel.add_widget(freedrawbtn)


        # toolpanel is added to holder grid
        col3.add_widget(toolpanel)

        #code that creates the layer control buttons
        layercontrol = GridLayout()
        layercontrol.cols=5
        layercontrol.size = (200,40)
        layercontrol.size_hint=(None,None)
        layercontrol.pos_hint =(1,None)
        newbtn=Button(text="New", font_size=10)
        upbtn=Button(text="Move \n Up", font_size=10)
        dwnbtn=Button(text="Move \n Down", font_size=10)
        renamebtn=Button(text="Rename", font_size=10)
        deletebtn=Button(text="Delete", font_size=10)

        # code that controls the functionality of the buttons (does stuff when pressed depending on the button)
        def layercontrolbuttons(instance):
            if instance.text=="New":
                self.backend.addlayer()
                if self.backend.active_layer < (self.backend.layerlist.get_stack_size()-2):
                    steps = self.backend.layerlist.get_stack_size()-1-self.backend.active_layer
                    steps +=-1
                    for i in range(steps):
                        self.backend.layerlist.swap_layers(self.backend.layerlist.get_layer(self.backend.layerlist.get_stack_size()-1-i),self.backend.layerlist.get_stack_size()-2-i)
                self.backend.active_layer+=1
                populatelayerlist()

            elif instance.text=="Move \n Up":
                if self.backend.active_layer > 1:
                    self.backend.layerlist.swap_layers(self.backend.layerlist.get_layer(self.backend.active_layer),self.backend.active_layer-1)
                    self.backend.set_active_layer(self.backend.active_layer-1)
                    populatelayerlist()
                    memorymap = CoreImage(self.backend.drawmap(),ext='png')
                    imw2.texture=memorymap.texture

            elif instance.text=="Move \n Down":
                if self.backend.active_layer < self.backend.layerlist.get_stack_size()-1:
                    self.backend.layerlist.swap_layers(self.backend.layerlist.get_layer(self.backend.active_layer),self.backend.active_layer+1)
                    self.backend.set_active_layer(self.backend.active_layer+1)
                    populatelayerlist()
                    memorymap = CoreImage(self.backend.drawmap(),ext='png')
                    imw2.texture=memorymap.texture

            elif instance.text=="Rename":
                def resolve(instance):
                    newname=instance.text
                    self.backend.layerlist.set_name(self.backend.active_layer,newname)
                    pop.dismiss()
                    populatelayerlist()
                input=TextInput(text="Layername")
                input.multiline=False
                input.bind(on_text_validate=resolve)
                pop=Popup(
                title="Rename Layer:",
                content=(input),
                size_hint=(None,None),
                size=(500,100),
                auto_dismiss = True
                )
                pop.open()
                populatelayerlist()

            elif instance.text=="Delete":
                if self.backend.layerlist.get_stack_size()>1:
                    self.backend.layerlist.del_layer(self.backend.active_layer)
                    self.backend.active_layer+=-1
                    populatelayerlist()
                    memorymap = CoreImage(self.backend.drawmap(),ext='png')
                    imw2.texture=memorymap.texture
        newbtn.bind(on_press=layercontrolbuttons)
        upbtn.bind(on_press=layercontrolbuttons)
        dwnbtn.bind(on_press=layercontrolbuttons)
        renamebtn.bind(on_press=layercontrolbuttons)
        deletebtn.bind(on_press=layercontrolbuttons)
        # adds buttons to widget
        layercontrol.add_widget(newbtn)
        layercontrol.add_widget(upbtn)
        layercontrol.add_widget(dwnbtn)
        layercontrol.add_widget(renamebtn)
        layercontrol.add_widget(deletebtn)
        col3.add_widget(layercontrol)

        # code to create the layer menue goes here
        layermenue = GridLayout()
        layermenue.padding = 5
        # code to generate background for layer menue
        with self.canvas.before:
            Color(0.5,0.5,0.5,1)
            layermenue.rect3 = Rectangle(pos=layermenue.pos,size=layermenue.size)

        layermenue.rows = 0
        layermenue.size=(200,400)
        layermenue.size_hint=(1,1)
        layermenue.pos_hint = (1,None)
        # buttons added here
        # this code handles the layer buttons being pressed
        def callback(instance):
            self.backend.set_active_layer(int(instance.text[-1]))
            print('The button <%s> is being pressed' % instance.text)
            populatelayerlist()
        # this code populates the layerlist gui with pressable layer buttons
        def populatelayerlist():
            layermenue.clear_widgets()
            listoflayers = self.backend.layerlist.get_layerlist()
            listoflayers
            for i in listoflayers[1:]:
                name = i.get_name() + " At position: "+str(self.backend.layerlist.get_layerlist().index(i))
                btn = Button(text=name)
                btn.bind(on_press=callback)
                layermenue.rows +=1
                if listoflayers.index(i) == self.backend.active_layer:
                    btn.background_normal =""
                    btn.color=(0,0,0,1)
                    #btn.background_color = (1,0.3,0.4,0.85)
                layermenue.add_widget(btn)
            try:
                self.backend.set_active_tileset(self.backend.layerlist.get_layer(self.backend.active_layer).get_tileset_file())
            except:
                print ("didn't change set")
            imw.source=self.backend.active_tile_set.get_targetfile()
            currentset.text=self.backend.active_tile_set.get_targetfile().split('/')[-1]
            imw.reload()
            self.backend.drawactivetile()
            imw3.reload()
        populatelayerlist()




        # added to holder grid here
        col3.add_widget(layermenue)

        # holder grid is added to main window here
        self.add_widget(col3)
        # bind command that updates the size and position of the toolpanel and layer objects
        toolpanel.bind(pos=toolpanelupdater,size= toolpanelupdater)
        layermenue.bind(pos=toolpanelupdater,size= toolpanelupdater)

        #method that updates the main window background
    def upd(self,instance,value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


#class that mitigates the recursion depth error when resizing.
class SafeScrollEffect(DampedScrollEffect):
    def __init__(self, **kwargs):
        super(SafeScrollEffect, self).__init__(**kwargs)
        self.on_value = Clock.create_trigger(
            super(SafeScrollEffect, self).on_value)









class Test(App):
    def build(self):
        return MainScreen()

if __name__=="__main__":
    Test().run()
