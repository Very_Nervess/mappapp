# a simple child of the Canvas class, the layer has a name and a tileset object in addition to the base properties of the canvas.
from Canvas import Canvas
from Tile import Tile

class Layer(Canvas):
    def __init__(self,name,width,height,tileset,gridresolution=64):
        Canvas.__init__(self,width,height,gridresolution)
        self.name = name
        self.tileset = tileset
# method to set the name of a layer
    def set_position(self,newname):
        self.name = newname
# method to get the name
    def get_name(self):
        return self.name
# method that reports the tilset's filename
    def get_tileset_file(self):
        return self.tileset.get_targetfile()
# function that updates the size of a layer. Any current tiles are preseved in the top left corner of the layer.
    def set_size(self,width,height):
        self.set_width(width)
        self.set_height(height)
        old_tiles = self.tiles[:]
        self.tiles = []
        numberofcolumns = width//self.gridresolution
        numberofrows = height//self.gridresolution
        for i in range(numberofcolumns):
            self.tiles.append([])
            for j in range(numberofrows):
                self.tiles[i].append(Tile())
        for i, col in enumerate(old_tiles):
            for j, tile in enumerate(col):
                x=tile.get_targetcol()
                y=tile.get_targetrow()
                if i<len(self.tiles) and j<len(self.tiles[0]):
                    self.set_tile(i,j,x,y)

    # a method that prints the layer information to a single string: each column of tiles is seperated by a $, and each tile by a # tiles are then stored accorded to (yco-ord,xco-ord). Mostly used for saving the layer to file.
    def to_string(self):
        result = ""
        result+=self.get_name()+"*"+self.tileset.get_targetfile()+"*"+str(self.get_width())+"*"+str(self.get_height())+"*"+str(self.get_gridres())+"*"
        for i in self.tiles:
            result+="$"
            for j in i:
                result+="#"+str(j.get_targetrow())+","+str(j.get_targetcol())
        return result

"""redundant code:"""
#ovveride method that alters the == method to check if two layers have the same position, if so they are considered to be equal
    #def __eq__(self, otherlayer):
    #    if self.get_name() == otherlayer.get_name():
        #    return True
    #    else: return False
#override method that alters the str method to return a string of the layer's perameters in order.
    #def __str__(self):
    #    return (str(self.get_name())+str(self.get_width())+str(self.get_height())+str(self.get_gridres()))
